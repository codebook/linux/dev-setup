# How to Set Up (Universal) Dev Env on Debian/Ubuntu Linux

_Tested on Ubuntu 16.04 LTS, 17.04, and 17.10._



## General Setup

    sudo apt install -y build-essential git nodejs npm curl xclip 
    sudo apt install -y emacs vim
    
I generally install vlc, imagemagick, gimp, and inkscape on my dev machines.

    sudo apt install -y vlc imagemagick graphicsmagick gimp inkscape ghostscript



Typescript is my "primary" programming language these days: 

    sudo npm i -g typescript


There are so many options, 
but my choice for the "programmer's editor" (or, a light-weight IDE) is Visual Studio Code at this point:

    wget -c https://go.microsoft.com/fwlink/?LinkID=760868
    sudo dpkg -i vsc.deb

_Note: I generally set the following user settings (or, something like this):_

    {
        "window.zoomLevel": 2,
        "editor.tabSize": 2,
        "files.exclude": {
            "**/.git": true,
            "**/.svn": true,
            "**/.hg": true,
            "**/CVS": true,
            "**/.DS_Store": true,
            "**/*.ngfactory.ts": true,
            "**/*.ngsummary.json": true,
            "**/.vscode": true
        },
        "editor.parameterHints": false,
        "workbench.editor.openPositioning": "first",
        "explorer.autoReveal": false,
        "typescript.check.tscVersion": false
    }

Also, custom keybindings:

    [
      {
        "key": "ctrl+k s",
        "command": "workbench.action.files.saveAll"
      },
    ]


Now, set up your git:

    git config --global user.name "your name"
    git config --global user.email "your email"
    git config --global push.default simple


## Setting up SSH key for GitLab

I prefer GitLab to GitHub or any other git hosting service. 
I recently moved all my personal projects (from GitHub, BitBucket, Visual Studio online) to GitLab.
Set up SSH for GitLab:

https://docs.gitlab.com/ce/ssh/README.html



## Installing Node

_Note: This is no longer necessary as of Ubuntu 17.10._

The Debian/Ubuntu package repository contains old versions of Node.js.
You will need to use a PPA to install the latestet version of Node, e.g., 6.x or 7.x.

    sudo apt install -y nodejs-legacy
    curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
    sudo apt install -y nodejs


## Cordova, Ionic

Hybrid apps are my new favorites.
I always install corodva and ionic to my dev computers.

    sudo npm i -g cordova ionic



Also: 
Angular, React, ....

    sudo npm i -g @angular/cli
    sudo npm i -g polymer-cli
    sudo npm i -g create-react-app




## Cmake, Clang

    sudo apt install -y cmake
    sudo apt install -y clang



## Python

Python (both python versions 2 and 3) comes with default Ubuntu installation.

    which python
    which python3
    sudo apt install -y python-pip
    sudo apt install -y python3-pip

When you do Python development, one of the first things you do is to create a virtual env.
Follow this doc... _tbd_



## Setting Up Java

First, download a JDK .tgz file from Oracle JDK download page.

...

    /opt/java/jdk1.8.xxx
    ...


Then, install ant, maven and gradle.
Note that these packages available on apt repository has dependency on OpenJava.
It's best to download binaries directly and set these up manually.


http://ant.apache.org/bindownload.cgi


http://maven.apache.org/download.cgi


https://gradle.org/releases/




Then update `/etc/profile` to add these in your path (and, for all other users in the system).


My /etc/profile looks like this.

    ...
    export JAVA_HOME=/opt/java/jdk-9.0.4
    export ANT_HOME=/opt/apache/apache-ant-1.10.1
    export MAVEN_HOME=/opt/apache/apache-maven-3.5.2
    export GRADLE_HOME=/opt/gradle/gradle-4.4.1
    export GROOVY_HOME=/opt/apache/groovy-2.4.13
    export GOROOT=/opt/go/go-1.9.2
    export KOTLIN_HOME=/opt/kotlin/kotlinc-1.1.51
    export SPRING_BOOT=/opt/spring/spring-1.5.9.RELEASE
    
    export PATH=$PATH:$JAVA_HOME/bin:$ANT_HOME/bin:$MAVEN_HOME/bin:$GRADLE_HOME/bin:$GROOVY_HOME/bin:$GOROOT/bin:$KOTLIN_HOME/bin:$SPRING_BOOT/bin
    ...




...



## IDE

Eclipse has been around for a looooong time, and I used it on and off for various purposes.
But, I'm not a big fan of Eclipse.

http://www.eclipse.org/downloads/



Alternatively, you can use a free version of IntelliJ from JetBrains or NetBeans from Oracle, etc.
I am currently experimenting with NetBeans (for Java development) and I like it (so far).
This is not your grandfather's NetBeans.
It seems that NetBeans has come a long way.

https://netbeans.org/downloads/

https://www.jetbrains.com/idea/download/#section=linux




If you do Java Spring development, Spring STS (which is based on Eclips) comes in handy.

https://spring.io/tools/sts/all





I generally install SDKs under shared locations via sudo (e.g., `/usr/local` or `/opt`),
and IDEs in my own home directory (e.g., under `~/opt`).



## Android

https://developer.android.com/studio/index.html


You can include the bin dirs in your path (e.g., in `~/.profile`). 
I just symlink the main executable to my bin dir and use the rest exectuables with full path.

    ln -s ~/opt/netbeans-8.2/bin/netbeans ~/bin/netbeans
    ln -s ~/opt/android-studio/bin/studio.sh ~/bin/studio
    ...



## .Net Core SDK

https://www.microsoft.com/net/learn/get-started/linuxubuntu


    curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
    sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg

    sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-ubuntu-artful-prod artful main" > /etc/apt/sources.list.d/dotnetdev.list'


    sudo apt update && sudo apt install -y dotnet-sdk-2.0.3
    dotnet --version



### VSC C# Extension:

https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp

Ctrl+P and

    ext install csharp

And, also do it for python, etc...




## Other Programming Languages

### GoLang

https://golang.org/dl/


### Lua

https://www.lua.org/download.html


### Kotlin

Kotlin is one of the JVM-based languages, which seems to be taking off 
(after Google adopted it as one of the official languages for Android development).
Kotlin programs can be transpiled into Java or Javascript.

https://github.com/JetBrains/kotlin/releases/
https://github.com/JetBrains/kotlin/releases/tag/v1.1.4-3


### Scala

Probably, the most popular JVM-based language, other than Java, is Scala, which supports functional programming.
You can install `sbt` (Scala Build Tool) via apt.

    echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
    sudo apt update && sudo apt install -y sbt

You can complete the installation by running a simple command:

    sbt --version



### Groovy


http://groovy-lang.org/download.html



### Spring Boot

Spring is not a language/platform, but it is one of the most popular Web frameworks in Java (and other JVM languages like Groovy and Kotlin) 
and you'll most likely want to set up dev env for Spring if you are a Java developer. 
The first thing to do, in my view, is to install Spring Boot CLI.
Although it's optional, it suits my dev style, and I do use Spring CLI for various tasks, including scaffolding new apps.

https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#getting-started-installing-the-cli




### PHP (CLI)

    sudo apt install -y php-cli 

PHP is a popular Web programming language. (I don't believe CLI is as popular.)
In order to do the Web development, you will need to install one of the Web frameworks in PHP
such as Symfony, CakePHP, Laravel, etc. _tbd_


### Ruby on Rails

...



### Swift

...




### Haskell

...



Etc. etc.

_tbd_




## AWS

http://docs.aws.amazon.com/cli/latest/userguide/installing.html

    sudo apt install -y awscli

    // ???
    // pip install awscli --upgrade --user

    aws --version


You can start by setting credentials and basic configs:

    aws configure


You can also install `aws-shell`, 
which provides some added benefits (like using a specific (non-default) profile 
for the duration of a session without having to use `--profile` for every command):

    sudo pip install aws-shell




## Google Cloud

https://cloud.google.com/sdk/downloads#apt-get


    export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
    echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
    sudo apt update && sudo apt install -y google-cloud-sdk
    gcloud --version


gcloud init ....



## Azure

https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest


    echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ wheezy main" |      sudo tee /etc/apt/sources.list.d/azure-cli.list
    sudo apt-key adv --keyserver packages.microsoft.com --recv-keys 52E16F86FEE04B979B07E28DB02C46DF417A0893
    sudo apt-get install apt-transport-https
    sudo apt update && sudo apt install -y azure-cli
    az --version




## Docker

https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/#install-using-the-repository

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo apt-key fingerprint 0EBFCD88
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"
    sudo apt update && sudo apt install -y docker-ce


    sudo docker run hello-world




## VirtualBox

https://www.virtualbox.org/wiki/Linux_Downloads

extension???




QEMU, KVM, etc...



## Database servers


MySQL

Postgress

MongoDB

CouchDB

Redis

DynamoDB local server





## Deep Learning Libraries


### TensorFlow


### MXNet


### CNTK


### Keras


### Kaffe



## Smart Assistants


### Alexa


### Cortana


### Google Assistants






## OpenCV

The best thing to do is to compile OpenCV with proper configurations for your needs.

How to build OpenCV:  _tbd_




## Unity3D

https://forum.unity3d.com/threads/unity-on-linux-release-notes-and-known-issues.350256/



## Unreal Engine

...





## tbd

...




